"""
Most cites provided didn't make it through the weather api. To get around this
I used an open backwards geolocator API to extract a lat,lng pair from a city
the weather api would then have no issue returning result from a lat,lng pair 
"""

import sqlalchemy as sql
from Cities import Cities
from base import Session
import requests
from opencage.geocoder import OpenCageGeocode


cage = OpenCageGeocode("b675c48a1e7844c89cb276eefa8c8860")
session = Session()

for city in session.query(Cities).all():

    print("City:", city.city)
    result = cage.geocode("{0},{1}".format(city.city, city.country), no_annotations='1')
    if result and len(result):
        print("Success")
        city.lat = result[0]['geometry']['lat']
        city.longi = result[0]['geometry']['lng']
    else:
        print("Failure")


session.commit()
