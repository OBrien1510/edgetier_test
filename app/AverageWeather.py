class AverageWeather:

    def __init__(self, weather):

        self.weather = weather

    def get_avg_temp(self, args):
        """
        Weather api returns results from mutliple different sources in a list each
        with their own temperture. This method calculates the average of all these
        tempertures

        Possible arguments: 'the', 'min', 'max'
        """
        if args not in ['the', 'min', 'max']:
            args = 'the'
        total = 0
        for i in self.weather["consolidated_weather"]:
            total += i["{0}_temp".format(args)]

        return total/len(self.weather["consolidated_weather"])

    def get_avg_des(self):
        """
        Use majority voting to decide which weather description to return
        """

        desc = dict()
        current_maj = ""
        current_max = 0

        for i in self.weather["consolidated_weather"]:

            if i["weather_state_name"] in desc.keys():
                desc[i["weather_state_name"]] += 1
            else:
                desc[i["weather_state_name"]] = 1

            if desc[i["weather_state_name"]] > current_max:
                current_max = desc[i["weather_state_name"]]
                current_maj = i["weather_state_name"]

            return current_maj

    def get_avg_pressure(self):

            total = 0
            for i in self.weather["consolidated_weather"]:
                total += i["air_pressure"]

            return total/len(self.weather["consolidated_weather"])


    def get_score(self, city):
        """
        (bars + museums)/population + air pressure + average temperture + public transport rank
        ---------------------------------------------------------------------------------------
                                crime rate * 100

        In general this formula seems to over penalize cities with very large populations
        """

        return ((city.bars + city.museums)/city.population + self.get_avg_pressure() + self.get_avg_temp("the") + city.public_transport)/(city.crime_rate*100)
