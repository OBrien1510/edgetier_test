from tornado.web import RequestHandler
from http import HTTPStatus
import json
from .base import Session
from .Cities import Cities
import sqlalchemy as sql
import requests
from .AverageWeather import AverageWeather


class LocationHandler(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()

    def get(self, *args, **kwargs):
        """
        Retrieve information about a city.
        :param args:
        :param kwargs:
        :return:
        """
        city = args[0]

        session = Session()


        city = session.query(Cities).filter_by(city = city.lower()).first()
        if city is None:
            self.set_status(HTTPStatus.BAD_REQUEST)
        weather_api = "https://www.metaweather.com/api/location/{0}/".format(city.weatherid)
        print(city.city)

        weather = requests.get(weather_api)

        if not weather:
            response = {"Error" : "Weather API fail",
            "City" : args[0]}
            self.set_status(HTTPStatus.OK)
            self.write(json.dumps(response))

        aw = AverageWeather(weather.json())


        response = {'city_name': city.city,
                    'current_temperature': aw.get_avg_temp("the"),
                    'current_weather_description': aw.get_avg_des(),
                    'population': city.population,
                    'bars': city.bars,
                    'city_score': aw.get_score(city)}

        print(response)

        self.set_status(HTTPStatus.OK)
        self.write(json.dumps(response))
