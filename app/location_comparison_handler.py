from tornado.web import RequestHandler
from http import HTTPStatus
import json
from .base import Session
from .Cities import Cities
import sqlalchemy as sql
import requests
from .AverageWeather import AverageWeather


class LocationComparisonHandler(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()

    def get(self, *args, **kwargs):
        """
        Retrieves information about multiple cities, rates them and returns a ranking and score for each city.
        :param args:
        :param kwargs:
        :return:
        """

        session = Session()
        max_score = 0
        cities = args[0].split(",")
        current_rank = 1
        ranked = list()

        for i in cities:
            print("city", i)
            city = session.query(Cities).filter_by(city = i.strip().lower()).first()
            weather_api = "https://www.metaweather.com/api/location/{0}/".format(city.weatherid)
            weather = requests.get(weather_api)

            if not weather:
                response = {"Error" : "Weather API fail",
                "City" : args[0]}
                self.set_status(HTTPStatus.OK)
                self.write(json.dumps(response))

            aw = AverageWeather(weather.json())

            city_ranked = {"city_name": city.city,
                           "city_rank": current_rank,
                           "city_score": aw.get_score(city)}

            ranked.append(city_ranked)
            current_rank += 1

        response = {'city_data': self.bubble_sort(ranked)}

        self.set_status(HTTPStatus.OK)
        self.write(json.dumps(response))

    def bubble_sort(self, ranked):

        """
        Simple bubble sort to sort the response dictionary into descending order by city score
        """
        sorted = False
        swapped = False
        print("ranked", ranked)
        while not sorted:
            current = ranked[0]
            swapped = False
            for i in range(1, len(ranked)):

                if ranked[i]["city_score"] > current["city_score"]:
                    swapped = True
                    ranked[i]["city_rank"] -= 1
                    current["city_rank"] += 1
                    ranked[i], ranked[i-1] = current, ranked[i]

                current = ranked[i]

            if swapped == False:

                sorted = True

        return ranked
