"""
Script to convert a cities approx lat,lng to its relevant weather id for the
weather api and subsequently store that weather id in the database for future use
"""

import sqlalchemy as sql
from Cities import Cities
from base import Session
import requests

session = Session()
print("querying")

weather_api = "https://www.metaweather.com/api/location/search/?lattlong="

for city in session.query(Cities).all():

    city_id = city.id

    weather = requests.get(weather_api+"{0},{1}".format(city.lat, city.longi))
    #print("requesting",city.city)
    if weather and len(weather.json()) != 0:
        weather = weather.json()
        #print(weather)
        city.weatherid = weather[0]["woeid"]
        print("Commiting", city.city)

    else:
        print("Failure for", city.city)
        print(weather.json())

session.commit()
