from sqlalchemy import Column, String, Integer, Float
from .base import Base

class Cities(Base):

    __tablename__ = "cities"

    id = Column(Integer, primary_key=True)
    city = Column(String)
    country = Column(String)
    population =  Column(Float)
    bars = Column(Integer)
    museums = Column(Integer)
    public_transport = Column(Integer)
    crime_rate = Column(Integer)
    average_hotel_cost = Column(Integer)
    index = Column(Integer)
    weatherid = Column(Integer)
    lat = Column(Float)
    longi = Column(Float)

    def __init__(city, country, population, bars, museums, public_transport, crime_rate,average_hotel_cost, index, weatherid,lat, longi):
        self.city = city
        self.country = country
        self.population = population
        self.bars = bars
        self.museums = museums
        self.public_transport = public_transport
        self.crime_rate = crime_rate
        self.average_hotel_cost = average_hotel_cost
        self.index = index
        self.weatherid = weatherid
        self.lat = lat
        self.longi = longi
